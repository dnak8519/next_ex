import styles from "./page.module.css";

export default function List() {
  return (
    <div className={styles.list}>
      <h4>상품목록</h4>
      <div className={styles.foodList}>
        <div className={styles.food}>
          <h1>상품1 $40</h1>
        </div>

        <div className={styles.food}>
          <h1>상품2 $40</h1>
        </div>
      </div>
    </div>
  );
}
